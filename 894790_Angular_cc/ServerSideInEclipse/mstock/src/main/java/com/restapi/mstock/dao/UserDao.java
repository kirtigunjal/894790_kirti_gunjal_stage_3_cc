package com.restapi.mstock.dao;

import com.restapi.mstock.models.UserDetails;

public interface UserDao {

	public UserDetails find(String email);
	
}
